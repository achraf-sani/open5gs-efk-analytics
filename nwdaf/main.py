from fastapi import FastAPI
from api.anomalies import router as anomalies_router
from api.performance import router as performance_router
from api.traffic import router as traffic_router

from api.analytics_subscription import routes as subscription_routes
from api.analytics_info import routes as analytics_routes

app = FastAPI(
    title="Nnwdaf_AnalyticsInfo",
    description="Nnwdaf_AnalyticsInfo Service API.", 
    version="0.1",
    servers=[
        {
            "url": "{apiRoot}/nnwdaf-analyticsinfo/v1",
            "description": "NWDAF AnalyticsInfo API",
            "variables": {
                "apiRoot": {
                    "default": "https://example.com",
                    "description": "apiRoot as defined in subclause 4.4 of 3GPP TS 29.501."
                }
            }
        }
    ]
)

app.include_router(subscription_routes.router, tags=["Subscriptions"])
app.include_router(analytics_routes.router, tags=["Analytics"])

@app.get("/", tags=["apiRoot"])
async def root():
    return {"message": "NWDAF API Root - navigate to /docs for API documentation"}