import httpx
import pytest

BASE_URL = "http://localhost:8084"

@pytest.fixture
def subscription_id():
    """Fixture to create a subscription and return its ID"""
    with httpx.Client() as client:
        response = client.post(f"{BASE_URL}/subscriptions", json={"details": "Monitor anomalies"})
        subscription = response.json()
        return subscription['id']

def test_create_subscription():
    """Test to verify that the subscription was created successfully"""
    assert isinstance(subscription_id, str)

def test_delete_subscription():
    """Test to verify that the subscription can be deleted"""
    with httpx.Client() as client:
        response = client.delete(f"{BASE_URL}/subscriptions/{subscription_id}")
        assert response.status_code == 200
        assert 'id' in response.json()
        assert 'details' in response.json()
        assert response.json()['id'] == str(subscription_id)