from pydantic import BaseModel, Field, UUID4
from typing import Optional, List, Any, Union
from enum import Enum
from uuid import UUID


class AnalyticsData(BaseModel):
    id: UUID
    details: str
    active: bool

class EventFilter(BaseModel):
    anySlice: str

class EventIdEnum(str, Enum):
    LOAD_LEVEL_INFORMATION = "LOAD_LEVEL_INFORMATION"
    NETWORK_PERFORMANCE = "NETWORK_PERFORMANCE"
    NF_LOAD = "NF_LOAD"
    SERVICE_EXPERIENCE = "SERVICE_EXPERIENCE"
    UE_MOBILITY = "UE_MOBILITY"
    UE_COMMUNICATION = "UE_COMMUNICATION"
    QOS_SUSTAINABILITY = "QOS_SUSTAINABILITY"
    ABNORMAL_BEHAVIOUR = "ABNORMAL_BEHAVIOUR"
    USER_DATA_CONGESTION = "USER_DATA_CONGESTION"
    NSI_LOAD_LEVEL = "NSI_LOAD_LEVEL"

class EventID(BaseModel):
    event_id: Union[EventIdEnum, str] = Field(
        ...,
        description=(
            "Possible values are\n"
            "- LOAD_LEVEL_INFORMATION: Represent the analytics of load level information of corresponding network slice.\n"
            "- NETWORK_PERFORMANCE: Represent the analytics of network performance information.\n"
            "- NF_LOAD: Indicates that the event subscribed is NF Load.\n"
            "- SERVICE_EXPERIENCE: Represent the analytics of service experience information of the specific applications.\n"
            "- UE_MOBILITY: Represent the analytics of UE mobility.\n"
            "- UE_COMMUNICATION: Represent the analytics of UE communication.\n"
            "- QOS_SUSTAINABILITY: Represent the analytics of QoS sustainability information in the certain area.\n"
            "- ABNORMAL_BEHAVIOUR: Indicates that the event subscribed is abnormal behaviour information.\n"
            "- USER_DATA_CONGESTION: Represent the analytics of the user data congestion in the certain area.\n"
            "- NSI_LOAD_LEVEL: Represent the analytics of Network Slice and the optionally associated Network Slice Instance."
        )
    )