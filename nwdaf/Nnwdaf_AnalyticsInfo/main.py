from fastapi import FastAPI
import routes as analytics_routes

app = FastAPI(
    title="Nnwdaf_AnalyticsInfo",
    description="Nnwdaf_AnalyticsInfo Service API.", 
    version="0.1",
    servers=[
        {
            "url": "{apiRoot}/nnwdaf-analyticsinfo/v1",
            "description": "NWDAF AnalyticsInfo API",
            "variables": {
                "apiRoot": {
                    "default": "https://example.com",
                    "description": "apiRoot as defined in subclause 4.4 of 3GPP TS 29.501."
                }
            }
        }
    ]
)

app.include_router(analytics_routes.router, tags=["AnalyticsInfo"])