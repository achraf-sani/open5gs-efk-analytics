from fastapi import APIRouter
from typing import List
from models.models import AnalyticsData, EventFilter, EventID

router = APIRouter()

# Dummy data
analytics_data = {
    "anomalies": [{"type": "traffic spike", "details": "Unexpected increase in usage"}],
    "performance": [{"metric": "latency", "value": "20ms"}]
}

@router.get("/analytics", response_model=List[AnalyticsData], summary="Read a NWDAF Analytics")
def get_analytics():
    return analytics_data["anomalies"]