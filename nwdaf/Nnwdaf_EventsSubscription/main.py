from fastapi import FastAPI
from app.routes import router as subscription_routes

app = FastAPI(
    title="Nnwdaf_EventsSubscription",
    description="Nnwdaf_EventsSubscription Service API.", 
    version="0.1",
    servers=[
        {
            "url": "{apiRoot}/nnwdaf-eventssubscription/v1",
            #"description": "NWDAF EventsSubscription API",
            "variables": {
                "apiRoot": {
                    "default": "https://example.com",
                    "description": "apiRoot as defined in subclause 4.4 of 3GPP TS 29.501."
                }
            }
        }
    ]
)

app.include_router(
    subscription_routes,
    prefix="/nnwdaf-eventssubscription/v1",
)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=80)