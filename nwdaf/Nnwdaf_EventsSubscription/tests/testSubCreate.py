from fastapi.testclient import TestClient
from app.main import app

client = TestClient(app)

def test_create_subscription():
    response = client.post("/subscriptions", json={"details": "Test subscription"})
    assert response.status_code == 200
    assert response.json()['details'] == "Test subscription"

def test_delete_subscription():
    # First, create a subscription to delete
    response = client.post("/subscriptions", json={"details": "Test subscription"})
    subscription_id = response.json()['id']

    # Now, delete the subscription
    delete_response = client.delete(f"/subscriptions/{subscription_id}")
    assert delete_response.status_code == 200
    assert delete_response.json()['id'] == subscription_id

def test_update_subscription():
    # First, create a subscription to update
    create_response = client.post("/subscriptions", json={"details": "Test subscription"})
    subscription_id = create_response.json()['id']

    # Now, update the subscription
    update_response = client.put(f"/subscriptions/{subscription_id}", json={"details": "Updated subscription"})
    assert update_response.status_code == 200
    assert update_response.json()['details'] == "Updated subscription"
    assert update_response.json()['active'] is False