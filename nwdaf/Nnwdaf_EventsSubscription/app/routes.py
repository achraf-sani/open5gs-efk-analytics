from fastapi import APIRouter, HTTPException
from uuid import uuid4, UUID
from app.models import (
    SubscriptionCreateRequest, 
    SubscriptionResponse, 
    SubscriptionUpdateRequest,
    NnwdafEventsSubscriptionNotification
)

router = APIRouter()

subscriptions = {}  # In-memory storage for subscriptions (PoC)
notifications = []  # In-memory storage for notifications (PoC)

@router.post(
        "/subscriptions",
        response_model=SubscriptionResponse,
        summary="Create a new Individual NWDAF Events Subscription",
        tags=["NWDAF Events Subscriptions (Collection)"],
)
async def create_subscription(subscription: SubscriptionCreateRequest):
    subscription_id = uuid4()
    subscriptions[subscription_id] = SubscriptionResponse(id=subscription_id, details=subscription.details, active=True)
    return subscriptions[subscription_id]

@router.get(
    "/subscriptions",
    response_model=list[SubscriptionResponse],
    summary="Get all NWDAF Events Subscriptions",
    tags=["NWDAF Events Subscriptions (Collection)"]
)
async def get_all_subscriptions():
    return list(subscriptions.values())

@router.delete(
        "/subscriptions/{subscription_id}",
        response_model=SubscriptionResponse,
        summary="Delete an existing Individual NWDAF Events Subscription",
        tags=["Individual NWDAF Events Subscription (Document)"]
)
async def delete_subscription(subscription_id: UUID):
    if subscription_id not in subscriptions:
        raise HTTPException(status_code=404, detail="Subscription not found")
    removed_subscription = subscriptions.pop(subscription_id)
    return removed_subscription

@router.put(
        "/subscriptions/{subscription_id}",
        response_model=SubscriptionResponse,
        summary="Update an existing Individual NWDAF Events Subscription",
        tags=["Individual NWDAF Events Subscription (Document)"]
)
async def update_subscription(subscription_id: UUID, subscription: SubscriptionUpdateRequest):
    if subscription_id not in subscriptions:
        raise HTTPException(status_code=404, detail="Subscription not found")
    updated = subscriptions[subscription_id].dict()
    update_data = subscription.dict(exclude_unset=True)
    updated.update(update_data)
    subscriptions[subscription_id] = SubscriptionResponse(**updated)
    return subscriptions[subscription_id]

@router.post(
    "/notifications",
    status_code=204,
    summary="Receive NWDAF Events Subscription Notification",
    tags=["NWDAF Events Subscriptions (Collection)"]
)
async def receive_notification(notification: NnwdafEventsSubscriptionNotification):
    notifications.append(notification)
    return
