from pydantic import BaseModel, Field, UUID4
from typing import Optional, List
from datetime import datetime
from uuid import UUID

class SubscriptionCreateRequest(BaseModel):
    details: str

class SubscriptionResponse(BaseModel):
    id: UUID
    details: str
    active: bool

class SubscriptionUpdateRequest(BaseModel):
    details: Optional[str] = None
    active: Optional[bool] = None

class EventNotification(BaseModel):
    sliceLoadLevelInfo: Optional[str] = None
    ueMobs: Optional[str] = None
    # Ajoutez d'autres champs spécifiques aux événements ici

class AnaMetaInfo(BaseModel):
    # Définissez les métadonnées d'analyse ici si nécessaire
    meta_data: str

class NnwdafEventsSubscriptionNotification(BaseModel):
    event: str
    eventNotifications: EventNotification
    anaMetaInfo: Optional[AnaMetaInfo] = None
    start: datetime
    expiration: datetime