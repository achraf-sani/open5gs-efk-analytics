from kafka import KafkaConsumer
from opensearchpy import OpenSearch
import json
import os

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

from datetime import datetime

def convert_unix_to_iso(unix_timestamp):
    """ Convert a UNIX timestamp (in seconds) to an ISO 8601 date format. """
    return datetime.utcfromtimestamp(unix_timestamp).isoformat() + 'Z'


# Read environment variables
KAFKA_SERVERS = os.getenv('KAFKA_BOOTSTRAP_SERVERS', 'kafka:9092').split(',')
OPENSEARCH_HOSTS = os.getenv('OPENSEARCH_HOSTS', 'opensearch-node1:9200').split(',')
OPENSEARCH_USER = os.getenv('OPENSEARCH_USER')
OPENSEARCH_PASSWORD = os.getenv('OPENSEARCH_PASSWORD')

logging.info("test")
# Set up the Kafka consumer
logging.info("Setting up Kafka Consumer...")
consumer = KafkaConsumer(
    'amf_log',
    bootstrap_servers=KAFKA_SERVERS,
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    value_deserializer=lambda x: json.loads(x.decode('utf-8'))
)

# OpenSearch client with HTTP 
opensearch_client = OpenSearch(
    hosts=[{'host': host.split(':')[0], 'port': int(host.split(':')[1])} for host in OPENSEARCH_HOSTS],
    http_auth=(OPENSEARCH_USER, OPENSEARCH_PASSWORD),
    use_ssl=True,
    verify_certs=False,
    ssl_shown_warn=False
)

if opensearch_client.ping():
    info = opensearch_client.info()
    logging.info(f"Welcome to {info['version']['distribution']} {info['version']['number']}!")
else:
    logging.error("Failed to connect to OpenSearch.")
    exit(1)  # Exits the script if cannot connect to OpenSearch


# Start consuming messages
logging.info("Starting to consume messages from Kafka...")
for message in consumer:
    doc = message.value
    logging.info(f"Received message: {doc}")
    doc['@timestamp'] = convert_unix_to_iso(doc['@timestamp'])

    # Index document in OpenSearch
    try:
        response = opensearch_client.index(index="open5gs-amf-logs", body=doc)
        print(f"Document indexed successfully: {response['_id']}")
    except Exception as e:
        print(f"Failed to index document: {e}")