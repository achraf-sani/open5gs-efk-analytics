from opensearchpy import OpenSearch
from opensearch_dsl import Search, Document, Text, Keyword
import os
import json
import requests
from dotenv import load_dotenv
from pathlib import Path

dotenv_path = Path('../../.env')
load_dotenv(dotenv_path=dotenv_path)

# Read environment variables
OPENSEARCH_HOSTS = os.getenv('OPENSEARCH_HOSTS', 'localhost:9200').split(',')
OPENSEARCH_USER = os.getenv('OPENSEARCH_USER')
OPENSEARCH_PASSWORD = os.getenv('OPENSEARCH_PASSWORD')

opensearch_client = OpenSearch(
    hosts=[{'host': host.split(':')[0], 'port': int(host.split(':')[1])} for host in OPENSEARCH_HOSTS],
    http_auth=(OPENSEARCH_USER, OPENSEARCH_PASSWORD),
    http_compress = True, # enables gzip compression for request bodies
    use_ssl=True,
    verify_certs=False,
    ssl_show_warn = False,
)

info = opensearch_client.info()
print(f"Welcome to {info['version']['distribution']} {info['version']['number']}!")


class AMFLog:
    index_name = 'open5gs-amf-logs'

    def __init__(self, timestamp, module, log_level, message):
        self.timestamp = timestamp
        self.module = module
        self.log_level = log_level
        self.message = message

    def save(self):
        body = {
            "@timestamp": self.timestamp,
            "module": self.module,
            "log_level": self.log_level,
            "message": self.message
        }
        response = opensearch_client.index(index=self.index_name, body=body)
        return response

# Create index if it doesn't exist
if not opensearch_client.indices.exists(index=AMFLog.index_name):
    index_body = {
        "settings": {
            "number_of_shards": 1,
            "number_of_replicas": 1
        },
        "mappings": {
            "properties": {
                "@timestamp": {"type": "date"},
                "module": {"type": "keyword"},
                "log_level": {"type": "keyword"},
                "message": {"type": "text"}
            }
        }
    }
    opensearch_client.indices.create(index=AMFLog.index_name, body=index_body)
    print("Index created.")
else:
    print("Index already exists.")



OPENSEARCH_DASHBOARDS_HOST = 'http://localhost:5601' 
INDEX_PATTERN_NAME = 'open5gs-*'

def check_index_pattern_exists():
    url = f"{OPENSEARCH_DASHBOARDS_HOST}/api/saved_objects/_find?type=index-pattern&search_fields=title&search={INDEX_PATTERN_NAME}"
    headers = {
        'kbn-xsrf': 'true',
    }
    response = requests.get(url, headers=headers, auth=(OPENSEARCH_USER, OPENSEARCH_PASSWORD))
    results = response.json()
    return len(results['saved_objects']) > 0

def create_index_pattern():
    headers = {
        'Content-Type': 'application/json',
        'osd-xsrf': 'true',  # Required for OpenSearch Dashboards APIs
    }
    data = {
        "attributes": {
            "title": INDEX_PATTERN_NAME,
            "timeFieldName": "@timestamp"
        }
    }

    if not check_index_pattern_exists():
        url = f"{OPENSEARCH_DASHBOARDS_HOST}/api/saved_objects/index-pattern"
        response = requests.post(url, headers=headers, auth=(OPENSEARCH_USER, OPENSEARCH_PASSWORD), data=json.dumps(data))
        return response.json()
    else:
        return "Index pattern already exists."

# Create index pattern
try:
    response = create_index_pattern()
    if isinstance(response, dict) and response.get('id'):
        print("Index pattern created successfully :", json.dumps(response, indent=4))
    elif isinstance(response, str):
        print(response)
    else:
        print("Error creating index pattern :", json.dumps(response, indent=4))
except Exception as e:
    print("Error connecting or creating index pattern : ", e)
