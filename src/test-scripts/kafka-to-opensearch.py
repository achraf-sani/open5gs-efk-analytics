from kafka import KafkaConsumer
from opensearchpy import OpenSearch
import json
import certifi

# Set up the Kafka consumer
print("Setting up Kafka Consumer...")
consumer = KafkaConsumer(
    'amf_log',
    bootstrap_servers=['localhost:29092'],
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    value_deserializer=lambda x: json.loads(x.decode('utf-8'))
)

# OpenSearch client with HTTP (adjust if using HTTPS)
opensearch_client = OpenSearch(
    hosts=[{'host': 'localhost', 'port': 9200}],
    http_auth=('admin', 'your_admin_password'),  # Use actual admin and password
    use_ssl=False,  # Set True if HTTPS is needed
    verify_certs=False,  # Set True to verify SSL certs
    ca_certs=certifi.where()  # Required if verify_certs is True
)

# Check connection status
if opensearch_client.ping():
    print("Connected to OpenSearch successfully!")
else:
    print("Failed to connect to OpenSearch.")

# Start consuming messages
print("Starting to consume messages from Kafka...")
for message in consumer:
    doc = message.value
    print(f"Received message: {doc}")

    # Index document in OpenSearch
    try:
        response = opensearch_client.index(index="your_index", document=doc)
        print(f"Document indexed successfully: {response['_id']}")
    except Exception as e:
        print(f"Failed to index document: {e}")
