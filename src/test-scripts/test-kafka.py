import json
from kafka import KafkaConsumer

# Topic configuration
LOGS_KAFKA_TOPIC = "amf_log"  

# Consumer setup
consumer = KafkaConsumer(
    LOGS_KAFKA_TOPIC,
    bootstrap_servers="localhost:29092", 
    auto_offset_reset='earliest',  # To read messages from the beginning of the topic
)

print("Listening for logs from Open5GS AMF...")
while True:
    for msg in consumer:
        log_entry = json.loads(msg.value.decode())
        print('{}'.format(json.dumps(log_entry, indent=4)))
        print()  
